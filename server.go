package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	fakedb "gitlab.com/joaonrb/bdd101/notes/modules/datastore/fake-db"
	"gitlab.com/joaonrb/bdd101/notes/server"
	"net/http"
	"time"
)

func main() {
	fmt.Println("Starting server at port 8080")
	access := log.New()
	access.SetFormatter(&log.JSONFormatter{})
	newServer := http.Server{
		Addr:           ":8080",
		Handler:        server.New(access, fakedb.New()),
		ReadTimeout:    30 * time.Second,
		WriteTimeout:   30 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	fmt.Println(newServer.ListenAndServe())
	fmt.Println("Stopping...")
}
