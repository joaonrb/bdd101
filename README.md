# BDD 101

This document will hopefully help understanding a bit the process to achieve Behaviour Driven
Development. It will obviously be a week example, since a single person will do it, instead of a
actual team.

## Note Tracker Project

The purpose is to create a tool to create, store and delete notes. This notes should have a title, a
creator, and a date of creation.

Simple enough.

## Running the tests

First, make sure you have python 3.7+ installed. Then, install pipenv. Last you can run the 
following command in the project path to install the environment:

```shell script
pipenv install --dev
```

To run the tests, just run:

```shell script
pipenv run behave
```