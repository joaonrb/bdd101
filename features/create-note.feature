# Created by joaonrb at 15/09/20
Feature: Create a note
  In order to create a note
  As a normal user
  I want to write a note with a title

  Scenario: Create a first note
    Given I write "Don't forget chocolates at FC"
    And I add the title "I like chocolate"
    When I create the note
    Then I have 1 note

  Scenario: Get the first note
    Given I have 1 note
    When I get the note with title "I like chocolate"
    Then I have a note that reads "Don't forget chocolates at FC"
    And the note is created by me
    And it was created recently

  Scenario: Delete the first note
    Given I have 1 note
    When I delete the note with title "I like chocolate"
    Then I have 0 note
