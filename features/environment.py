"""
Behave environment.

2020-09-15 joaonrb
"""
from steps.rest_api.create_note import Note


def before_feature(context, feature):
    context.editing_note = Note()
    context.note = None
