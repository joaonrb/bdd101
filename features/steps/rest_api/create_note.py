"""
This defines the steps for the features.

2020-09-15 joaonrb
"""
import requests
import http
from dataclasses import dataclass
from typing import Optional
from datetime import datetime, timedelta
from .settings import rest_api_server
from behave import *


@dataclass
class Note:
    title: Optional[str] = None
    text: Optional[str] = None
    creator: Optional[str] = None
    created_at: Optional[datetime] = None


users = {
    "me": "joao",
    "I": "joao"
}


@given("I write \"{text}\"")
def step_impl(context, text):
    context.editing_note.text = text


@given("I add the title \"{title}\"")
def step_impl(context, title):
    context.editing_note.title = title


@when("{user} create the note")
def step_impl(context, user):
    body = {
        "user": users.get(user),
        "title": context.editing_note.title,
        "text": context.editing_note.text,
    }
    context.response = requests.post(f"{rest_api_server}/note", json=body)
    assert context.response.status_code == http.HTTPStatus.CREATED, "failed to create note"


@when("{user} get the note with title \"{title}\"")
def step_impl(context, user, title):
    params = {
        "user": users.get(user),
        "title": context.editing_note.title,
    }
    context.response = requests.get(f"{rest_api_server}/note", params=params)
    assert context.response.status_code == http.HTTPStatus.OK, "failed to get note"
    body = context.response.json()
    body["created_at"] = datetime.strptime(body["created_at"], "%Y-%m-%d %H:%M:%S")
    context.note = Note(**body)


@when("{user} delete the note with title \"{title}\"")
def step_impl(context, user, title):
    params = {
        "user": users.get(user),
        "title": context.editing_note.title,
    }
    context.response = requests.delete(f"{rest_api_server}/note", params=params)
    assert context.response.status_code == http.HTTPStatus.ACCEPTED, "failed to delete note"


@given("{user} have {number} note")
@then("{user} have {number} note")
def step_impl(context, user, number):
    params = {
        "user": users.get(user),
        "title": context.editing_note.title,
    }
    context.response = requests.get(f"{rest_api_server}/notes/count", params=params)
    assert context.response.status_code == http.HTTPStatus.OK, "failed to get note count"
    assert context.response.text == number, \
        f"got number {context.response.text} instead of expected {number}"


@then("I have a note that reads \"{text}\"")
def step_impl(context, text):
    assert context.note.text == text


@then("the note is created by {user}")
def step_impl(context, user):
    assert context.note.creator == users.get(user)


@then("it was created recently")
def step_impl(context):
    assert context.note.created_at-datetime.now() < timedelta(seconds=10)

