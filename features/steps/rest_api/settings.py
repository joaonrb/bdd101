"""
Rest API settings for steps.

2020-09-20 joaonrb
"""
import os

rest_api_server = os.environ.get("BDD_101_TARGET", "http://localhost:8080")
