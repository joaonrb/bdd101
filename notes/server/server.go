package server

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/joaonrb/bdd101/notes/core/interfaces"
	"net/http"
	"strings"
	"time"
)

type Handler struct {
	access    *log.Logger
	datastore interfaces.DataStore
}

func New(access *log.Logger, datastore interfaces.DataStore) *Handler {
	return &Handler{access: access, datastore: datastore}
}

func (h *Handler) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	start := time.Now()

	var body string
	var status int
	switch request.URL.Path {
	case "/note":
		body, status = h.Note(request)
	case "/notes/count":
		body, status = h.NoteCount(request)
	default:
		body, status = http.StatusText(http.StatusNotFound), http.StatusNotFound
	}
	response.WriteHeader(status)
	response.Write([]byte(body))
	h.access.WithFields(log.Fields{
		"method":         request.Method,
		"status":         status,
		"bytesServed":    len(body),
		"ip":             request.RemoteAddr,
		"path":           request.URL.Path,
		"parameters":     request.URL.RawQuery,
		"duration":       time.Now().UTC().Sub(start).String(),
		"durationMicros": time.Now().UTC().Sub(start),
	}).Println(strings.Trim(http.StatusText(status), " "))
}
