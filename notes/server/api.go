package server

import (
	"encoding/json"
	"fmt"
	usecases "gitlab.com/joaonrb/bdd101/notes/core/use-cases"
	"net/http"
	"time"
)

type JSONTime time.Time

func (t JSONTime) MarshalJSON() ([]byte, error) {
	//do your serializing here
	stamp := fmt.Sprintf("\"%s\"", time.Time(t).Format("2006-01-02 15:04:05"))
	return []byte(stamp), nil
}

type Note struct {
	Title     string   `json:"title"`
	Text      string   `json:"text"`
	Creator   string   `json:"creator"`
	User      string   `json:"user,omitempty"`
	CreatedAt JSONTime `json:"created_at,omitempty"`
}

func (h *Handler) Note(request *http.Request) (string, int) {
	switch request.Method {
	case http.MethodGet:
		note, err := usecases.GetNote(h.datastore, request.URL.Query().Get("title"))
		if err != nil {
			return http.StatusText(http.StatusNotFound), http.StatusNotFound
		}
		bodyNote := Note{
			Title:     note.Title(),
			Text:      note.Text(),
			Creator:   note.Creator(),
			CreatedAt: JSONTime(note.CreatedAt()),
		}
		body, _ := json.Marshal(bodyNote)
		return string(body), http.StatusOK
	case http.MethodPost:
		var note Note
		err := json.NewDecoder(request.Body).Decode(&note)
		if err != nil {
			return http.StatusText(http.StatusBadRequest), http.StatusBadRequest
		}
		_, err = usecases.CreateNote(h.datastore, note.Title, note.Text, note.User)
		if err != nil {
			return http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError
		}
		return http.StatusText(http.StatusCreated), http.StatusCreated
	case http.MethodDelete:
		err := usecases.DeleteNote(h.datastore, request.URL.Query().Get("title"))
		if err != nil {
			return http.StatusText(http.StatusNotFound), http.StatusNotFound
		}
		return http.StatusText(http.StatusAccepted), http.StatusAccepted
	}
	return http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed
}

func (h *Handler) NoteCount(request *http.Request) (string, int) {
	switch request.Method {
	case http.MethodGet:
		number := usecases.CountNotes(h.datastore, request.URL.Query().Get("user"))
		return fmt.Sprintf("%d", number), http.StatusOK
	}
	return http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed
}
