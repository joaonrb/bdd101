package usecases

import (
	"gitlab.com/joaonrb/bdd101/notes/core/interfaces"
)

// DeleteNote delete a note in the datastore.
func DeleteNote(datastore interfaces.DataStore, title string) error {
	return datastore.DeleteNote(title)
}
