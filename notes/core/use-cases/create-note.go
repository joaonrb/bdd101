package usecases

import (
	"gitlab.com/joaonrb/bdd101/notes/core/entities"
	"gitlab.com/joaonrb/bdd101/notes/core/entities/note"
	"gitlab.com/joaonrb/bdd101/notes/core/interfaces"
	"time"
)

// CreateNote create a note in the datastore.
func CreateNote(datastore interfaces.DataStore, title, text, user string) (entities.Note, error) {
	n := note.New(title, text, user, time.Now())
	return n, datastore.PutNote(n)
}
