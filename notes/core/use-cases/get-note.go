package usecases

import (
	"gitlab.com/joaonrb/bdd101/notes/core/entities"
	"gitlab.com/joaonrb/bdd101/notes/core/interfaces"
)

// GetNote get a note in the datastore.
func GetNote(datastore interfaces.DataStore, title string) (entities.Note, error) {
	return datastore.GetNote(title)
}
