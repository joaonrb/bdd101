package usecases

import (
	"gitlab.com/joaonrb/bdd101/notes/core/interfaces"
)

// CountNotes count the notes in the datastore for an user.
func CountNotes(datastore interfaces.DataStore, user string) int {
	return datastore.CountNotes(user)
}
