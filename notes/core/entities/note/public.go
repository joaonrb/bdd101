package note

import (
	"gitlab.com/joaonrb/bdd101/notes/core/entities"
	"time"
)

// New constructs the note
func New(title, text, creator string, createdAt time.Time) entities.Note {
	return &note{title, text, creator, createdAt}
}
