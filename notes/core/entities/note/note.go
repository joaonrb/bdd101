package note

import "time"

type note struct {
	title     string
	text      string
	creator   string
	createdAt time.Time
}

// Title return the note title.
func (n *note) Title() string {
	return n.title
}

// Text return the note content.
func (n *note) Text() string {
	return n.text
}

// Creator return the note creator.
func (n *note) Creator() string {
	return n.creator
}

// CreatedAt return the created time.
func (n *note) CreatedAt() time.Time {
	return n.createdAt
}
