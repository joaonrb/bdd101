package entities

import "time"

// Note is an interface for the note
type Note interface {
	Title() string
	Text() string
	Creator() string
	CreatedAt() time.Time
}
