package errors

import "fmt"

type NotFoundError struct {
	item string
}

func NotFound(item string) *NotFoundError {
	return &NotFoundError{item: item}
}

func (e *NotFoundError) Error() string {
	return fmt.Sprintf("%s not found", e.item)
}
