package interfaces

import "gitlab.com/joaonrb/bdd101/notes/core/entities"

// DataStore defines a database behaviour.
type DataStore interface {
	GetNote(title string) (entities.Note, error)
	PutNote(note entities.Note) error
	DeleteNote(title string) error
	CountNotes(user string) int
}
