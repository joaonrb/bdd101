package fakedb

import (
	"gitlab.com/joaonrb/bdd101/notes/core/entities"
	"gitlab.com/joaonrb/bdd101/notes/core/interfaces"
)

// New construct the fake DB.
func New() interfaces.DataStore {
	return &fakeDB{noteByTitleStore: map[string]entities.Note{}, noteByUserStore: map[string][]entities.Note{}}
}
