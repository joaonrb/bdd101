package fakedb

import (
	"gitlab.com/joaonrb/bdd101/notes/core/entities"
	"gitlab.com/joaonrb/bdd101/notes/core/errors"
	"sync"
)

type fakeDB struct {
	noteByTitleStore map[string]entities.Note
	noteByUserStore  map[string][]entities.Note
	mux              sync.Mutex
}

func (db *fakeDB) GetNote(title string) (entities.Note, error) {
	n, ok := db.noteByTitleStore[title]
	if ok {
		return n, nil
	}
	return nil, errors.NotFound(title)
}

func (db *fakeDB) PutNote(note entities.Note) error {
	db.mux.Lock()
	defer db.mux.Unlock()
	db.noteByTitleStore[note.Title()] = note
	_, ok := db.noteByUserStore[note.Creator()]
	if ok {
		db.noteByUserStore[note.Creator()] = append(db.noteByUserStore[note.Creator()], note)
	} else {
		db.noteByUserStore[note.Creator()] = []entities.Note{note}
	}
	return nil
}

func (db *fakeDB) DeleteNote(title string) error {
	db.mux.Lock()
	defer db.mux.Unlock()
	note, err := db.GetNote(title)
	if err == nil {
		delete(db.noteByTitleStore, title)
		notes := db.noteByUserStore[note.Creator()]
		db.noteByUserStore[note.Creator()] = []entities.Note{}
		for _, note := range notes {
			if note.Title() != title {
				db.noteByUserStore[note.Creator()] = append(db.noteByUserStore[note.Creator()], note)
			}
		}
	}

	return nil
}

func (db *fakeDB) CountNotes(user string) int {
	db.mux.Lock()
	defer db.mux.Unlock()
	n, ok := db.noteByUserStore[user]
	if ok {
		return len(n)
	}
	return 0
}
